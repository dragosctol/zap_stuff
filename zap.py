#!/usr/bin/env python

import time
import json
from pprint import pprint
from zapv2 import ZAPv2
import getopt
import sys
import os,subprocess,signal,psutil

from collections import defaultdict
from random import randrange
from prettytable import PrettyTable




def usage():
    print 'usage -c <config>'

def get_conf_file():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hc:", ["help", "config="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    config_file_path = "config.default.json"
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-c", "--config"):
            config_file_path = a
        else:
            assert False, "unhandled option"
    return config_file_path


def load_config(config_file_path):
    with open(config_file_path) as config_file:
        config = json.load(config_file)
    return config


def check_zap_proxy_running(cmdline):
    for proc in psutil.process_iter():
        if proc.cmdline == cmdline:
            print "Zap scanner running. Exiting ... "
            sys.exit(4)
    print "No zap running. Conitinue ... "


def start_zap_proxy(config_proxy_start, config_proxy_script):
    if  config_proxy_start == True:
        print 'Starting ZAP ...'
        print config_proxy_script
        proc = subprocess.Popen([config_proxy_script,'-daemon'],stdout=open(os.devnull,'w'))
        print 'Waiting for ZAP to load, 10 seconds ...'
        time.sleep(10)
        return proc.pid
    else:
        print "Skipping proxy start ... "
        return False


def do_scanning(config):

    check_zap_proxy_running(config['start_proxy_cmdline'])
    zap_pid = start_zap_proxy(config['start_proxy_daemon'],config['start_proxy_script'])

    zap=ZAPv2()
    target = config['target']
    print 'Accessing target %s' % target
    zap.urlopen(target)
    time.sleep(2)
  
    print "Target set : "+target


    #config spider
    zap.spider.set_option_max_depth(3)
    zap.spider.set_option_handle_parameters("IGNORE_VALUE")
    zap.spider.set_option_thread_count(24)
    #config scanner
    zap.ascan.set_option_thread_per_host(8) 

    #configure excludes
    for proxy_exclude_url in config['exclude_spider']:
        zap.core.exclude_from_proxy(proxy_exclude_url)
        print "Excluded from proxy : "+proxy_exclude_url

    for scan_exclude_url in config['exclude_scan']:
        zap.core.exclude_from_proxy(scan_exclude_url)
        print "Excluded from scanner : "+scan_exclude_url

    #start spider
    print 'Starting spidering the target %s' % target

    zap.spider.scan(target)
    time.sleep(2)
    while (int(zap.spider.status()) < 100):
        print 'Spider progress %: ' + zap.spider.status()
        time.sleep(10)
    print 'Zap Spider phase completed'
    time.sleep(5)
    #start scanner
    print 'Scanning vulnerabilities on target %s' % target
    zap.ascan.scan(target)
    while (int(zap.ascan.status()) < 100):
        print 'Scan progress %: ' + zap.ascan.status()
        time.sleep(20)
    print 'Scan completed'

    #print reports
    print 'Hosts: ' + ', '.join(zap.core.hosts)
    print 'Alerts: '
    #pprint (zap.core.alerts())

    print_reports(zap)

    print "Shutdown the zap ..."
    zap.core.shutdown()
    time.sleep(10)

    print "Trying to kill the zap anyway ... "
    try:
        os.kill(zap_pid, signal.SIGKILL)
    except:
        pass


def print_reports(zap):

    sort_by_url = defaultdict(list)
    for alert in zap.core.alerts():
        sort_by_url[alert['url']].append({
                                'risk':  alert['risk'],
                                'alert': alert['alert'],
                                'url': alert['url'],
                                'param': alert['param']
                                })

    # print a useful set of tables of the alerts
    for url in sort_by_url:
        print
        print url

        results = PrettyTable(["Risk", "Description", "URL", "Param"])
        results.padding_width = 1
        results.align = "l"
        results.sortby = "Risk"

        for details in sort_by_url[url]:
            results.add_row([details['risk'], details['alert'], details['url'], details['param']])
        print results

# main starts here
if __name__ == "__main__":
    conf_file_path = get_conf_file()
    config = load_config(conf_file_path)
    do_scanning(config)
